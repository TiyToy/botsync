# BotSync

Objecitf:
- Le bot Zignaly fait les achats sur l'exchange avec les providers
- Le bot Profit Trailer s'occupe de la vente

Fonctionnement - **Un trade est considéré comme terminé si il n'est plus visible dans PT**:
  - Connexion à Zingaly, récupération de l'ensemble des trades ouverts
  - Connexion à PT, récupération de l'ensemble des trades ouverts (Pairs et DCA)
  - Comparaison des données
  - Les trades encore présents dans Zingaly mais plus dans PT seront "cancel" ou clos dans Zignaly (car considéré comme vendu par PT)


- [BotSync](#botsync)
  - [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
  - [Crontab for automatic execution](#crontab-for-automatic-execution)
  - [Variable explication](#variable-explication)


## Getting Started

<span style="color:red"> Le premier lancement va "clore" l'ensemble des trades sur Zignaly </span>

<span style="color:red"> Pour évité ça, vous pouvez supprimé les TP et TS de Zig, PT va récupérer la main sur ces trades </span>

Let's go, remplacer /opt par votre votre répertoire de destination

```bash
INSTALL_DIRECTORY=/opt/
cd $INSTALL_DIRECTORY
apt update && apt install git python3
git clone https://gitlab.com/TiyToy/botsync.git
cd botsync
cp botsync/template_config.py botsync/config.py
echo "EDITE YOUR CONFIGFILE $INSTALL_DIRECTORY/botsync/config.py"
echo "lunch with: python3 botsync/main.py"
```

### Prerequisites

Install and Copy the config file from template
```
apt update && apt install git python3
git clone https://gitlab.com/TiyToy/botsync.git
cp botsync/template_config.py botsync/config.py
```
and complete ..
```sh
closePosition = False

# Actuellement, botsync regarde le PAIRS LOG et le DCA LOG.
# - Si une paire est trouvé, PT ne la donc pas encore vendu, elle ne sera donc pas supprimé de Zig.
# - Si dca_enable = True (ci-dessous) - Botsync va:
#     => Regarde les trades en cours dans le PAIRS LOG de PT
#     => Regarde si la paire dans le DCA LOG n'a pas atteint son "MAX COST REACHED" (ex DEFAULT_DCA_max_cost = 0.1)
#     => Donc, si la paire est dans le DCA LOG et n'a pas atteint son MAX COST REACHED, elle est clos dans Zig afin de permettre un nouveau achat
#  - Si un nouveau signal arrive dans Zig:, et qu'il n'est pas full paires
#      => Zig fait l'achat comme un nouveau trade
#      => PT fait la moyenne des deux trades (ça nous fait donc une petite DCA de type 100/50/33/...)

dca_enabled = False

zignaly = {
    "host" : "zignaly.com",
    "projectId" :"z01",
    "email": "",
    "password": "",
    "quote": "BTC",
    "providerManaged": ['Wavetrend Signals']
}

pt = {
    "host": "xx.xx.xxx.xxx:8081",
    "token": ""
}
```

**for close the Zignaly trade, set closPostion at True**


## Crontab for automatic execution

Ajouter la ligne suivante à la crontab pour une execution toutes les 5 minutes
crontab -e
```
*/5 * * * * cd /opt/botsync && python3 botsync/main.py
```


## Variable explication

* closePosition
  * "False" - Affiche les trades qui seront clos dans Zig
  * "True" - Dans Zig, ferme les trades vendu par PT

* dca_enabled

```bash
# Actuellement, botsync regarde le PAIRS LOG et le DCA LOG.
# - Si une paire est trouvé, PT ne la donc pas encore vendu, elle ne sera donc pas supprimé de Zig.
# - Si dca_enable = True (ci-dessous) - Botsync va:
#     => Regarde les trades en cours dans le PAIRS LOG de PT
#     => Regarde si la paire dans le DCA LOG n'a pas atteint son "MAX COST REACHED" (ex DEFAULT_DCA_max_cost = 0.1)
#     => Donc, si la paire est dans le DCA LOG et n'a pas atteint son MAX COST REACHED, elle est clos dans Zig afin de permettre un nouveau achat
#  - Si un nouveau signal arrive dans Zig:, et qu'il n'est pas full paires
#      => Zig fait l'achat comme un nouveau trade
#      => PT fait la moyenne des deux trades (ça nous fait donc une petite DCA de type 100/50/33/...)
```

* max_cost
```bash
# max_cost => Max Price for one piece in DCA
```

* max_pairs
```bash
# max_pairs => Nombre de paire max dans PT (Pair + DCA)
# Si max_pairs est atteint, que dca_enabled est True, et que le max_cost n'est pas atteint, on ajoute la paire à la whitelist afin de pouvoir être DCA
```