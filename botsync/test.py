# coding: utf-8

import http.client
import json
import ssl
import sys
import logging

log = logging.getLogger(__name__)
out_hdlr = logging.StreamHandler(sys.stdout)
out_hdlr.setFormatter(logging.Formatter('%(asctime)s %(message)s'))
out_hdlr.setLevel(logging.INFO)
log.addHandler(out_hdlr)
log.setLevel(logging.INFO)

zig = [{"positionId":"5ca87","base":"QLC"},{"positionId":"5ca86","base":"INS"},{"positionId":"5ca86","base":"WAVES"}]
pt = [{"currency": "QLC", "base": "BTC"},{"currency": "INS", "base": "BTC"}]


delta = list(
    {zig['base'] for zig in zig} -
    {pt['currency'] for pt in pt}
)
print(delta)

providerList = list(filter(lambda d: d["base"] in delta, zig))
print(providerList)

log.info(providerList)


a = ['CLOAKBTC', 'MODBTC', 'SALTBTC', 'SUBBTC', 'EDOBTC', 'XVGBTC', 'ETCBTC']

an = ','.join(a)
print(an)
print(str(a))