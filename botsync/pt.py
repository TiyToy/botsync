# coding: utf-8

import http.client
import json
import ssl
import os
import logging

import config
import common

logger = logging.getLogger('botsync')

def core():
    """ Pilot des interactions avec PT """
    
    ptPairsList = []
    ptDcaList = []
    whitelistZig = []

    ptPairsList = common.request('GET', config.pt['host'], '/api/pairs/log?token=' + config.pt['token'])
    ptDcaList = common.request('GET', config.pt['host'], '/api/dca/log?token='  + config.pt['token'])

    for dcaTrade in ptDcaList:
        if dcaTrade['totalCost'] < config.max_cost and config.dca_enabled is True:
            whitelistZig.append(dcaTrade['market'])
        else:
            logger.info('Actuel cost for {} in DCA {}/{} - Remove form Zig whitelist'.format(dcaTrade['currency'], dcaTrade['totalCost'], config.max_cost))
 
    ptTradeList = ptPairsList + ptDcaList

    return ptTradeList, sorted(whitelistZig)