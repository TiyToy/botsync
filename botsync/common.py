# coding: utf-8

import sys
import os
import http.client
import json
import ssl

import config
import logging

logger = logging.getLogger('botsync')

def request(method, server, url):
    """Execute an HTTP REST request to a server which exposes an API

    :param method: GET / POST etc
    :param server: FQDN
    :param url: path to get data ex : /api/5.0/ontap/clusters
    :param auth_basic: user/password in base64,
    ex: echo 'username:password' | base64
    :return: API result
    """

    headers = {
        'Accept': "application/json",
        'Cache-Control': "no-cache",
    }

    try:
        conn = http.client.HTTPSConnection(server, context=ssl._create_unverified_context())
        conn.request(method, url, headers=headers)

    except(IOError, http.client.HTTPException):
        conn = http.client.HTTPConnection(server)
        conn.request(method, url, headers=headers)

    res = conn.getresponse()
    json_data = res.read().decode("utf-8")
    try:
        data = json.loads(json_data)
    except ValueError:
        logger.debug('{} response {}'.format(url,json_data))
        sys.exit

    return(data)

def config_var_checkup(config):
    """ Vérification des paramètres des vars """

    if not hasattr(config, 'closePosition'):
        logger.info('Defind closePosition value, check the template_config')
        exit(0)

    if not hasattr(config, 'dca_enabled'):
        logger.info('Defind dca_enabled value, check the template_config')
        exit(0)

    if config.dca_enabled is True and not hasattr(config, 'max_cost'):
        logger.info('Defind max_cost value, check the template_config')
        exit(0)

    if config.dca_enabled is True and not hasattr(config, 'max_pairs'):
        logger.info('Defind max_pairs value, check the template_config')
        exit(0)

def config_dict_checkup(dictionary):
    """ Vérification des paramètres des dicts """
    for k, v in dictionary.items():
        if not v:
            print("Config ", k, " is empty")
            sys.exit(2)


def dict_filter(data, key, valueList):
    """ Dictionary filtering by list value match """
    if not data:
        providerList = list(filter(lambda d: d[key] in valueList, data))
    else:
        providerList = data

    return providerList

def div_protected(dividende, diviseur):

    if dividende > 0:
        y = float(dividende / diviseur)
    else:
        y = 0
    
    return y

def zig_return(data):

    if type(data) == bool and data == False:
        newdata = 0
    else:
        newdata = data

    return newdata