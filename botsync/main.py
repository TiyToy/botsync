#!/usr/bin/env python
# -*- coding: utf-8 -*-
# -*- Mode: Python; tab-width: 4 -*-

import sys
import os
import json
import logging
# import inspect
# import jsonpickle

import config
import common
import zignaly
import pt

# Log Setup
logger = logging.getLogger('botsync')
logger.setLevel(logging.INFO)

# create a logging format
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# create a file handler
handlerFile = logging.FileHandler('syncbot.log')
handlerFile.setLevel(logging.INFO)
handlerFile.setFormatter(formatter)
# create a stdout handler
handlerStdout = logging.StreamHandler(sys.stdout)
handlerStdout.setLevel(logging.INFO)
handlerStdout.setFormatter(formatter)
# add the handlers to the logger

logger.addHandler(handlerFile)
logger.addHandler(handlerStdout) 

def main():
    """ Core function """

    zigMaxPaire = 0

    # Verification du ficher de config
    common.config_dict_checkup(config.zignaly)
    common.config_dict_checkup(config.pt)
    common.config_var_checkup(config)

    # PT Get data
    ptTradeList, zigWhitelist = pt.core()

    # Zignaly
    zig_login_dict = zignaly.login(config.zignaly['host'], config.zignaly['email'], config.zignaly['password'], config.zignaly['projectId'])
    
    token = zig_login_dict['token']
    userId = zig_login_dict['userId']

    # Zig Manage position
    positionIdNeedCancel = zignaly.manage_open_position(token, zigWhitelist)

    if positionIdNeedCancel:
        logger.info('PT Trade closed: {}'.format(positionIdNeedCancel))
        zignaly.close_position(token, positionIdList=positionIdNeedCancel)   
    else:
        logger.info("Zignaly No postition need cancel")

    # Zig - Settings manage (changement du max paire pour toujours avoir avec PT full paire)
    if len(ptTradeList) < config.max_pairs:
        zigMaxPaire = config.max_pairs - len(ptTradeList)
        zigWhitelist = []

        logger.info('Zig set max pairs: {} and withlist: {}'.format(zigMaxPaire, zigWhitelist))
    else:
        zigMaxPaire = 0
        zigWhitelist = ','.join(zigWhitelist)

        logger.info('Zig set max pairs: {} and withlist: {}'.format(zigMaxPaire, zigWhitelist))

    zignaly.manage_settings(token=token, userId=userId, maxPositions=zigMaxPaire, whitelist=zigWhitelist)


if __name__ == "__main__":
    # execute only if run as a script
    main()