# coding: utf-8

import http.client
import json
import ssl
import os
import sys

import logging
import config
import common

logger = logging.getLogger('botsync')

def login(host, email, password, projectId):
    conn = http.client.HTTPSConnection(host)
    payload = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"email\"\r\n\r\n" + email +"\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"password\"\r\n\r\n"+ password +"\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"projectId\"\r\n\r\nz01\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--"

    headers = {
        'content-type': "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
        'Authorization': "Bearer 03405dff0cec8151e0465bac28f5637e",
        'cache-control': "no-cache",
        'Postman-Token': "5b7cbe3a-e17e-4b4c-9e03-6ebc4161ad42"
        }

    conn.request("POST", "/api/fe/api.php?action=login", payload, headers)

    res = conn.getresponse()
    json_data = res.read().decode("utf-8")
    data = json.loads(json_data)

    if res.status != 200:
        print("Connexion error: ", data)
        sys.exit(2)

    return(data)

def close_position(token, positionIdList):

    closePosition = []

    for positionId in positionIdList:
        url = '/api/fe/api.php?action=closePosition&token=' + token + '&positionId=' + positionId
        urluser = 'https://zignaly.com/app/position/' + positionId

        logger.info('Show the position: {}'.format(urluser))
        logger.info('API url for close: {}'.format(url))

        if config.closePosition is True:
            res = common.request('POST', config.zignaly['host'], url)
            closePosition.append(res)
            
            logger.info('NB close position:{}'.format(len(closePosition)))

            # History
            with open('closePositionHistory.json', 'a+') as file:
                file.write(json.dumps(closePosition))

        elif config.closePosition is False:
            logger.info('closePosition is disable - NB need position close:{}'.format(len(positionIdList)))

    return closePosition

def manage_open_position(token, zigWhitelist):

    tradeList = []
    positionIdNeedCancel = []

    getOpenPositions = common.request('GET', config.zignaly['host'], '/api/fe/api.php?action=getOpenPositions&token=' + token)

    if type(getOpenPositions) is not list:
        logger.info('Zig is empty:{}'.format(getOpenPositions))
        tradeList.append(getOpenPositions)
        # exit(0)

    elif type(getOpenPositions) is list:
        # Zignaly - Filtre des providers géré par PT
        tradeList = common.dict_filter(getOpenPositions, 'provider', config.zignaly['providerManaged'])
        tradeList = common.dict_filter(tradeList, 'quote', config.zignaly['quote'])

        logger.info('Zig NB Trade filtering:{}/{}'.format(len(tradeList),len(getOpenPositions)))

        # Cloture des postions Zig si paire en DCA PT pour nouvel achat sur signal du provider
        for zigTrade in tradeList:
            if zigTrade['symbol'] in zigWhitelist:
                positionIdNeedCancel.append(zigTrade['positionId'])

    return positionIdNeedCancel


def manage_settings(token, userId, maxPositions, whitelist):
    """ Update Provider settings """

    providerConnected = common.request('GET', config.zignaly['host'], '/api/fe/api.php?action=getProviderList&type=connected&token=' + token)

    for provider in providerConnected:
        currentSettings = {}
        newSettings = {}

        if provider['name'] in config.zignaly['providerManaged']:
            url = '/api/fe/api.php?action=getProvider&providerId=' + provider['id'] + '&token=' + token
            currentSettings = common.request('GET', config.zignaly['host'], url)         

            for exchange in currentSettings['exchange']:
                exchangeId = exchange

            if type(currentSettings['exchange'][exchangeId]['blacklist']) == list:
                newBlackList = ','.join(currentSettings['exchange'][exchangeId]['blacklist'])
            else:
                newBlackList = currentSettings['exchange'][exchangeId]['blacklist']

            newSettings = {
                'token' : token,
                'exchangeId' : exchange,
                'name' : currentSettings['exchange'][exchangeId]['name'],
                'trailingStopTrigger' : currentSettings['exchange'][exchangeId]['trailingStopTrigger'],
                'trailingStop' : currentSettings['exchange'][exchangeId]['trailingStop'],
                'stopLoss' : currentSettings['exchange'][exchangeId]['stopLoss'],
                'buyTTL' : int(common.div_protected(currentSettings['exchange'][exchangeId]['buyTTL'], 60)),
                'sellByTTL' : int(common.div_protected(currentSettings['exchange'][exchangeId]['sellByTTL'], 60)),
                'maxPositions' : maxPositions,
                'minVolume' : currentSettings['exchange'][exchangeId]['minVolume'],
                'positionsPerMarket' : int(currentSettings['exchange'][exchangeId]['positionsPerMarket']),
                'priceDeviation' : currentSettings['exchange'][exchangeId]['priceDeviation'],
                'takeProfitTargets' : currentSettings['exchange'][exchangeId]['takeProfitTargets'],
                'reBuyTargets' : currentSettings['exchange'][exchangeId]['reBuyTargets'],
                'blacklist' : newBlackList,
                'whitelist' : whitelist,
                'disable' : currentSettings['exchange'][exchangeId]['disable'],
                'positionSize' : 0,
                'positionSizeBTCValue' : float(currentSettings['exchange'][exchangeId]['positionsSize']['BTC']['value']),
                'positionSizeBTCUnit' : currentSettings['exchange'][exchangeId]['positionsSize']['BTC']['unit'],
                'positionSizeETHValue' : float(currentSettings['exchange'][exchangeId]['positionsSize']['ETH']['value']),
                'positionSizeETHUnit' : currentSettings['exchange'][exchangeId]['positionsSize']['ETH']['unit'],
                'positionSizeBNBValue' : float(currentSettings['exchange'][exchangeId]['positionsSize']['BNB']['value']),
                'positionSizeBNBUnit' : currentSettings['exchange'][exchangeId]['positionsSize']['BNB']['unit'],
                'positionSizeUSDTValue' : float(currentSettings['exchange'][exchangeId]['positionsSize']['USDT']['value']),
                'positionSizeUSDTUnit' : currentSettings['exchange'][exchangeId]['positionsSize']['USDT']['unit'],
                'positionSizePAXValue' : float(currentSettings['exchange'][exchangeId]['positionsSize']['PAX']['value']),
                'positionSizePAXUnit' : currentSettings['exchange'][exchangeId]['positionsSize']['PAX']['unit'],
                'positionSizeUSDCValue' : float(currentSettings['exchange'][exchangeId]['positionsSize']['USDC']['value']),
                'positionSizeUSDCUnit' : currentSettings['exchange'][exchangeId]['positionsSize']['USDC']['unit'],
                'positionSizeTUSDValue' : float(currentSettings['exchange'][exchangeId]['positionsSize']['TUSD']['value']),
                'positionSizeTUSDUnit' : currentSettings['exchange'][exchangeId]['positionsSize']['TUSD']['unit'],
                'positionSizeXRPValue' : float(currentSettings['exchange'][exchangeId]['positionsSize']['XRP']['value']),
                'positionSizeXRPUnit' : currentSettings['exchange'][exchangeId]['positionsSize']['XRP']['unit'],
                'positionSizeUSDSValue' : float(currentSettings['exchange'][exchangeId]['positionsSize']['USDS']['value']),
                'positionSizeUSDSUnit' : currentSettings['exchange'][exchangeId]['positionsSize']['USDS']['unit'],
                'providerId' : currentSettings['id']
                }

    
            conn = http.client.HTTPSConnection(config.zignaly['host'])
            payload = json.dumps(newSettings)

            headers = {
                'content-type': "application/json"
                }

            conn.request("POST", "/api/fe/api.php?action=updateProviderExchangeSettingsNew&token=" + token, payload, headers)

            res = conn.getresponse()
            json_data = res.read().decode("utf-8")
            data = json.loads(json_data)

            if res.status != 200:
                print("Connexion error: ", data)
                sys.exit(2)