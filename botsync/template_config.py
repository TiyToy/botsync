# closePosition (False / True): Active la cloture par le script des positions dans Zig si PT à vendu la paire
closePosition = False

# dca_enabled = False / True
# Actuellement, botsync regarde le PAIRS LOG et le DCA LOG.
# - Si une paire est trouvé, PT ne la donc pas encore vendu, elle ne sera donc pas supprimé de Zig.
# - Si dca_enable = True (ci-dessous) - Botsync va:
#     => Regarde les trades en cours dans le PAIRS LOG de PT
#     => Regarde si la paire dans le DCA LOG n'a pas atteint son "max_cost" (ex DEFAULT_DCA_max_cost = 0.1)
#     => Donc, si la paire est dans le DCA LOG et n'a pas atteint son MAX COST REACHED, elle est clos dans Zig afin de permettre un nouveau achat (like DCA par signal)
#  - Si un nouveau signal arrive dans Zig: et qu'il n'est pas full paires
#      => Zig fait l'achat comme un nouveau trade
#      => PT fait la moyenne des deux trades (ça nous fait donc une petite DCA de type 100/50/33/...)

dca_enabled = False

# max_cost => Max Price for one piece in DCA
max_cost = 0.03

# max_pairs => Nombre de paire max dans PT (Pair + DCA)
# Si max_pairs est atteint, que dca_enabled est True, et que le max_cost n'est pas atteint, on ajoute la paire à la whitelist afin de pouvoir être DCA
max_pairs = 5

zignaly = {
    "host" : "zignaly.com",
    "projectId" :"z01",
    "email": "",
    "password": "",
    "quote": "BTC",
    "providerManaged": ['Wavetrend Signals']
}

pt = {
    "host": "xx.xx.xxx.xxx:8081",
    "token": ""
}